from fastapi import APIRouter, Depends
from typing import Annotated
from fastapi.security import HTTPBasic, HTTPBasicCredentials

router = APIRouter(prefix="/auth", tags=["Auth"])

# security = HTTPBasic()
# @router.get("/basic-auth/")
# def basic_auth_credentials(
#         credentials: Annotated[HTTPBasicCredentials, Depends(security)]
# ):
#     return {
#         "message": "hi",
#         "username": "username",
#         "password": "password"
#
#     }