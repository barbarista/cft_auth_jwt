from fastapi import APIRouter, Depends
from sqlalchemy.ext.asyncio import AsyncSession
from starlette import status
from api_v1.user.schemas import User, UserCreate
from core.models import db_helper
from users.crud import CreateUser
from . import crud
from . import schemas


router = APIRouter(prefix="/users", tags=["Users"])


@router.post("/", response_model=CreateUser)
async def create_user(session, user_in: CreateUser):   #"CreateUser"
    return await crud.create_user(session=session, user_in=user_in)

@router.get("/", response_model=list[User])
async def get_users(
    session: AsyncSession = Depends(db_helper.scoped_session_dependency),
):
    return await crud.get_users(session=session)
# @router.post(
#     "/",
#     response_model=CreateUser,
#     status_code=status.HTTP_201_CREATED,
# )
# async def create_product(
#     user_in: CreateUser,
#     session: AsyncSession = Depends(db_helper.scoped_session_dependency),
# ):
#     return await crud.create_user(session=session, user_in=user_in)
