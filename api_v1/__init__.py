from fastapi import APIRouter
from .auth.views import router as auth_router
from .auth.jwt_auth import router as auth_jwt_router

auth_router.include_router(auth_jwt_router)
router = APIRouter()


router.include_router(router=auth_router)
router.include_router(router=auth_jwt_router)
