from typing import Annotated
from annotated_types import MinLen, MaxLen
from pydantic import BaseModel, EmailStr, ConfigDict

class UserBase(BaseModel):
    name: str
    password: str
class CreateUser(UserBase):
    pass
class User(UserBase):
    model_config = ConfigDict(strict=True)

    id: int

