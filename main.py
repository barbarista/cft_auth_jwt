from contextlib import asynccontextmanager
from core.models import Base, db_helper
import uvicorn
from fastapi import FastAPI
from fastapi import Depends, APIRouter
from api_v1.auth.views import router as auth_router
from users.views import router as users_router
from api_v1.user.crud import get_users
router = APIRouter()

@asynccontextmanager
async def lifespan(app: FastAPI):
    async with db_helper.engine.connect() as conn:

        await conn.run_sync(Base.metadata.create_all)

    yield

app = FastAPI(lifespan=lifespan)
app.include_router(auth_router)
app.include_router(users_router)




# @app.get("/", tags=["Index"])
# def hello_index():
#     return{
#         "message": "Hi"
#     }
#




if __name__ == '__main__':
    uvicorn.run("main:app", reload=True)
