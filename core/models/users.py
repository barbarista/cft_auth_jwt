import datetime
import email

from sqlalchemy import func, ForeignKey
from sqlalchemy.orm import Mapped, MappedColumn, mapped_column

from .base import Base


# class User(Base):
#     __tablename__ = "User"
#
#     username: Mapped[str]
#     password: Mapped[str]


class User(Base):
    __tablename__ = "users"

    id: Mapped[int] = mapped_column(primary_key=True, index=True)
    username: Mapped[str] = mapped_column(unique=True, index=True)
    hashed_password: Mapped[str]
class Salary(Base):
    __tablename__ = 'salaries'
    user_id: Mapped[int] = mapped_column(ForeignKey('users.id'), primary_key=True, index=True)
    salary: Mapped[float]
    next_raise: Mapped[datetime.datetime] = mapped_column(server_default=func.now())
